package com.newmaster.turistapp.mapas;

import android.graphics.Color;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.newmaster.turistapp.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.PolyUtil;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.newmaster.turistapp.data.crud.HotelCrud;
import com.newmaster.turistapp.hoteles.HotelDetailFragment;
import com.newmaster.turistapp.hoteles.modelo.ModeloHotel;
import com.newmaster.turistapp.volley.VolleyTurisApp;


public class RutaHotelActivity extends FragmentActivity implements OnMapReadyCallback
{
    private GoogleMap mMap;
    private LatLng origen;
    private LatLng destino;
    private HotelCrud hotelCrud;
    private ModeloHotel.Hotel mItem;
    private VolleyTurisApp volleyTurisApp;
    protected RequestQueue fRequestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ruta_hotel);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_ruta);
        Bundle bundleHotel = getIntent().getExtras();
        this.hotelCrud = HotelCrud.getInstance(this.getApplicationContext());
        this.volleyTurisApp = VolleyTurisApp.getInstance(this);
        this.fRequestQueue = this.volleyTurisApp.getRequestQueue();
        mItem = ModeloHotel.ITEM_MAP.get(getIntent().getStringExtra(HotelDetailFragment.ARG_ITEM_ID));
        this.destino = new LatLng(Double.parseDouble(mItem.latitud), Double.parseDouble(mItem.longitud));
        Log.e("Lat destino", mItem.latitud);
        Log.e("Lon destino", mItem.longitud);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        mMap = googleMap;
        mMap.clear();

        /*if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
        {
            return;
        }

        //mMap.setMyLocationEnabled(true);
        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        assert lm != null;
        Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        //Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        double longitud = location.getLongitude();
        double latitud = location.getLatitude();
        this.origen = new LatLng(latitud, longitud);*/
        this.origen = new LatLng(4.8047737, -75.7487812);
        this.requestRute();
        mMap.addMarker(new MarkerOptions().position(this.origen).title("Origen"));
        mMap.addMarker(new MarkerOptions().position(this.destino).title("Destino"));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(this.origen, 12));
    }

    public void addToQueue(Request request)
    {
        if (request != null)
        {
            request.setTag(this);

            if (this.fRequestQueue == null)
            {
                this.fRequestQueue = this.volleyTurisApp.getRequestQueue();
            }

            request.setRetryPolicy(new DefaultRetryPolicy(60000, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            this.fRequestQueue.add(request);
        }
    }

    public void requestRute()
    {
        String url = "https://maps.googleapis.com/maps/api/directions/json?origin=" +
                     this.origen.latitude + "," + this.origen.longitude +
                     "&destination=" + this.destino.latitude + "," + this.destino.longitude +
                     "&mode=walking";

        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>()
        {
            @Override
            public void onResponse(String response)
            {
                Gson gson = new GsonBuilder().create();
                JsonObject object = gson.fromJson(response, JsonElement.class).getAsJsonObject();
                String points = object.get("routes").getAsJsonArray().get(0).getAsJsonObject().get("overview_polyline").getAsJsonObject().get("points").getAsString();
                PolylineOptions polylineOptions = new PolylineOptions().color(Color.BLUE).width(5).addAll(PolyUtil.decode(points));
                mMap.addPolyline(polylineOptions);
            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                Toast.makeText(RutaHotelActivity.this, "Sin conexión", Toast.LENGTH_SHORT).show();
            }
        });

        this.addToQueue(request);
    }
}

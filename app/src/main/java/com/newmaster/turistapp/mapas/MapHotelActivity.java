package com.newmaster.turistapp.mapas;

import android.database.Cursor;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.newmaster.turistapp.R;
import com.newmaster.turistapp.data.crud.HotelCrud;
import com.newmaster.turistapp.hoteles.modelo.ModeloHotel;

import java.util.ArrayList;

public class MapHotelActivity extends FragmentActivity implements OnMapReadyCallback
{
    private GoogleMap mMap;
    private HotelCrud hotelCrud;
    private ArrayList<ModeloHotel.Hotel> listaHoteles;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_hotel);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        this.hotelCrud = HotelCrud.getInstance(this.getApplicationContext());
        this.listaHoteles = this.cursorToArray(this.hotelCrud.buscarHoteles());
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        mMap = googleMap;
        // Add a marker in Sydney and move the camera
        for (ModeloHotel.Hotel hotel: this.listaHoteles)
        {
            String lat = hotel.latitud;
            String lon = hotel.longitud;
            LatLng punto = new LatLng(Double.parseDouble(lat), Double.parseDouble(lon));
            mMap.addMarker(new MarkerOptions().position(punto).title(hotel.nombre));
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom((punto), 10));
        }

    }

    private ArrayList<ModeloHotel.Hotel> cursorToArray(Cursor cursor)
    {
        ModeloHotel.Hotel hotel;
        ArrayList<ModeloHotel.Hotel> listaHoteles = new ArrayList<>();

        while (cursor.moveToNext())
        {
            hotel = new ModeloHotel.Hotel(cursor);
            listaHoteles.add(hotel);
        }

        return listaHoteles;
    }
}

package com.newmaster.turistapp.splash;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.newmaster.turistapp.R;
import com.newmaster.turistapp.menu.MenuActivity;

public class SplashScreenActivity extends AppCompatActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        new Handler().postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                Intent intentMenu = new Intent(SplashScreenActivity.this, MenuActivity.class);
                startActivity(intentMenu);
            }
        }, 3000);
    }
}

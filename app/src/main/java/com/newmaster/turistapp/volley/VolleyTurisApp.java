package com.newmaster.turistapp.volley;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class VolleyTurisApp
{
    // Atributos
    private static VolleyTurisApp singleton;
    private RequestQueue requestQueue;
    private static Context context;

    private VolleyTurisApp(Context context)
    {
        VolleyTurisApp.context = context;
        requestQueue = getRequestQueue();
    }

    public static synchronized VolleyTurisApp getInstance(Context context)
    {
        if (singleton == null)
        {
            singleton = new VolleyTurisApp(context);
        }

        return singleton;
    }

    public RequestQueue getRequestQueue()
    {
        if (requestQueue == null)
        {
            requestQueue = Volley.newRequestQueue(context.getApplicationContext());
        }

        return requestQueue;
    }

    public  void addToRequestQueue(Request req)
    {
        getRequestQueue().add(req);
    }
}

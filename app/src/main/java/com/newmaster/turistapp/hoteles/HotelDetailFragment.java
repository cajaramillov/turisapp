package com.newmaster.turistapp.hoteles;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.newmaster.turistapp.R;
import com.newmaster.turistapp.hoteles.modelo.ModeloHotel;
import com.newmaster.turistapp.mapas.RutaHotelActivity;

/**
 * A fragment representing a single Hotel detail screen.
 * This fragment is either contained in a {@link HotelListActivity}
 * in two-pane mode (on tablets) or a {@link HotelDetailActivity}
 * on handsets.
 */
public class HotelDetailFragment extends Fragment
{
    /**
     * The fragment argument representing the item ID that this fragment
     * represents.
     */
    public static final String ARG_ITEM_ID = "item_id";

    /**
     * The dummy content this fragment is presenting.
     */
    private ModeloHotel.Hotel mItem;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public HotelDetailFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(ARG_ITEM_ID))
        {
            // Load the dummy content specified by the fragment
            // arguments. In a real-world scenario, use a Loader
            // to load content from a content provider.
            mItem = ModeloHotel.ITEM_MAP.get(getArguments().getString(ARG_ITEM_ID));
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View rootView = inflater.inflate(R.layout.hotel_detail, container, false);

        // Show the dummy content as text in a TextView.
        if (mItem != null)
        {
            ((TextView) rootView.findViewById(R.id.tv_hotel_nombre)).setText(mItem.nombre);
            ((TextView) rootView.findViewById(R.id.tv_hotel_descripcion)).setText(mItem.descripcionlarga);
            ((ImageView) rootView.findViewById(R.id.iv_hotel)).setImageResource(mItem.imagen);
        }

        FloatingActionButton fab = (FloatingActionButton) rootView.findViewById(R.id.fab_ruta);
        fab.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent intentMaps = new Intent(getActivity(), RutaHotelActivity.class);
                intentMaps.putExtra(HotelDetailFragment.ARG_ITEM_ID, mItem.id);
                startActivity(intentMaps);
            }
        });

        return rootView;
    }
}

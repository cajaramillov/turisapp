package com.newmaster.turistapp.hoteles.modelo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.newmaster.turistapp.data.TurisAppContract.HotelEntity;
import com.newmaster.turistapp.data.crud.HotelCrud;

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 * <p>
 * TODO: Replace all uses of this class before publishing your app.
 */
public class ModeloHotel
{
    private HotelCrud hotelCrud;
    private static ModeloHotel modeloHotel = null;

    /**
     * An array of sample (dummy) items.
     */
    public static final List<Hotel> ITEMS = new ArrayList<Hotel>();

    /**
     * A map of sample (dummy) items, by ID.
     */
    public static final Map<String, Hotel> ITEM_MAP = new HashMap<String, Hotel>();


    private ModeloHotel(Context context)
    {
        this.hotelCrud = HotelCrud.getInstance(context);
        this.cargarHoteles();
    }

    public static ModeloHotel getInstance(Context contexto)
    {
        if (modeloHotel == null)
        {
            modeloHotel = new ModeloHotel(contexto);
        }

        return modeloHotel;
    }

    private void cargarHoteles()
    {
        Cursor cursorHoteles = this.hotelCrud.buscarHoteles();

        while (cursorHoteles.moveToNext())
        {
            Hotel hotel = new Hotel(cursorHoteles);
            agregarHotel(hotel);
        }
    }


    @NonNull
    private static String generarId()
    {
        return UUID.randomUUID().toString();
    }

    private static void agregarHotel(Hotel hotel)
    {
        ITEMS.add(hotel);
        ITEM_MAP.put(hotel.id, hotel);
    }

    public static class Hotel
    {
        public final String id;
        public final String nombre;
        public final String descripcionCorta;
        public final String descripcionlarga;
        public final String ubicacion;
        public final String latitud;
        public final String longitud;
        public final int imagen;

        public Hotel(String nombre, String descCorta, String descLarga, String lalitud, String longitud, String ubicacion, int imagen)
        {
            this.id = ModeloHotel.generarId();
            this.nombre = nombre;
            this.descripcionCorta = descCorta;
            this.descripcionlarga = descLarga;
            this.latitud = lalitud;
            this.longitud = longitud;
            this.ubicacion = ubicacion;
            this.imagen = imagen;
        }

        public Hotel(Cursor cursor)
        {
            this.id = cursor.getString(cursor.getColumnIndex(HotelEntity._ID));
            this.nombre = cursor.getString(cursor.getColumnIndex(HotelEntity.NOMBRE));
            this.descripcionCorta = cursor.getString(cursor.getColumnIndex(HotelEntity.DESC_CORTA));
            this.descripcionlarga = cursor.getString(cursor.getColumnIndex(HotelEntity.DESC_LARGA));
            this.latitud = cursor.getString(cursor.getColumnIndex(HotelEntity.LATITUD));
            this.longitud = cursor.getString(cursor.getColumnIndex(HotelEntity.LONGITUD));
            this.ubicacion = cursor.getString(cursor.getColumnIndex(HotelEntity.UBICACION));
            this.imagen = cursor.getInt(cursor.getColumnIndex(HotelEntity.IMAGEN));

        }

        public ContentValues toContentValues()
        {
            ContentValues values = new ContentValues();
            values.put(HotelEntity.NOMBRE, this.getNombre());
            values.put(HotelEntity.DESC_CORTA, this.getDescripcionCorta());
            values.put(HotelEntity.DESC_LARGA, this.getDescripcionlarga());
            values.put(HotelEntity.LATITUD, this.getLatitud());
            values.put(HotelEntity.LONGITUD, this.getLongitud());
            values.put(HotelEntity.UBICACION, this.getUbicacion());
            values.put(HotelEntity.IMAGEN, this.getImagen());
            return  values;
        }

        public String getId() {
            return id;
        }

        public String getNombre() {
            return nombre;
        }

        public String getDescripcionCorta() {
            return descripcionCorta;
        }

        public String getDescripcionlarga() {
            return descripcionlarga;
        }

        public String getUbicacion() {
            return ubicacion;
        }

        public String getLatitud() {
            return latitud;
        }

        public String getLongitud() {
            return longitud;
        }

        public int getImagen() {
            return imagen;
        }


    }
}

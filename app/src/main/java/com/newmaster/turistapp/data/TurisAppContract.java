package com.newmaster.turistapp.data;

import android.provider.BaseColumns;

public class TurisAppContract
{
    public static abstract class HotelEntity implements BaseColumns
    {
        public static final String NOMBRE = "nombre";
        public static final String DESC_CORTA = "desc_corta";
        public static final String DESC_LARGA = "desc_larga";
        public static final String LATITUD = "latitud";
        public static final String LONGITUD = "longitud";
        public static final String UBICACION = "ubicacion";
        public static final String IMAGEN = "imagen";
    }
}

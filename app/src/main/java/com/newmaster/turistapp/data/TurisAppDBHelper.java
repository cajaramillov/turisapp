package com.newmaster.turistapp.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;

import com.newmaster.turistapp.R;
import com.newmaster.turistapp.data.TurisAppContract.HotelEntity;
import com.newmaster.turistapp.hoteles.modelo.ModeloHotel;

public class TurisAppDBHelper extends SQLiteOpenHelper
{
    private static final String DATA_BASE_NAME = "turisapp.db";
    private static final int VERSION = 1;

    public TurisAppDBHelper(Context contexto)
    {
        super(contexto, DATA_BASE_NAME, null, VERSION);
    }

    public interface Tables
    {
        String HOTEL = "hotel";
    }

    @Override
    public void onOpen(SQLiteDatabase db)
    {
        super.onOpen(db);

        if (!db.isReadOnly())
        {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
            {
                db.setForeignKeyConstraintsEnabled(true);
            }

            else
            {
                db.execSQL("PRAGMA foreign_keys=ON");
            }
        }
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase)
    {
        // Table SCORE_PLAYER
        sqLiteDatabase.execSQL(String.format("CREATE TABLE %s (" +
                                             "%s INTEGER PRIMARY KEY AUTOINCREMENT," +
                                             "%s TEXT NOT NULL," +
                                             "%s TEXT NOT NULL," +
                                             "%s TEXT NOT NULL," +
                                             "%s TEXT NOT NULL," +
                                             "%s TEXT NOT NULL," +
                                             "%s TEXT NOT NULL," +
                                             "%s INTEGER NOT NULL)",
                                             Tables.HOTEL,
                                             HotelEntity._ID,
                                             HotelEntity.NOMBRE,
                                             HotelEntity.DESC_CORTA,
                                             HotelEntity.DESC_LARGA,
                                             HotelEntity.LATITUD,
                                             HotelEntity.LONGITUD,
                                             HotelEntity.UBICACION,
                                             HotelEntity.IMAGEN));

        loadDataHotel(sqLiteDatabase);// Table SCORE_PLAYER

    }

    private void loadDataHotel(SQLiteDatabase db)
    {
        loadHotel(db, new ModeloHotel.Hotel(
                "Hotel Bolivar Plaza",
                "Categoría del hotel:3,5 estrella(s), Cantidad de habitaciones: 18 ",
                "Hotel sencillo, sin mayores pretensiones pero bien ubicado, cerca del centro de armenia, con facilidad de acceso para visitantes de negocios. Adecuado para pasar una o dos noches, es acogedor y el servicio es muy bueno. Dirección Calle 21a No. 14-17, Armenia",
                "4.45234778794663",
                "-75.78196047823484",
                "Dirección: Calle 21a No. 14-17, Armenia",
                R.drawable.bolivar_plaza));

        loadHotel(db, new ModeloHotel.Hotel(
                "Allure Aroma Mocawa Hotel",
                "Categoría del hotel:3,5 estrella(s), Cantidad de habitaciones: 97",
                "Disfruta de un hotel de excelente calidad y con un sello personal que lo hace ser único. Con una atención cinco estrellas, lo hace una de las mejores alternativas en hospedaje es Armenia. Con una perfecta ubicación y contar con un buen restaurante y un centro comercial, Dirección Carrera 14 No. 9N-00, Armenia",
                "4.447555758701706",
                "-75.78938483278806",
                "Dirección: Carrera 14 No. 9N-00, Armenia",
                R.drawable.mocawa));

        loadHotel(db, new ModeloHotel.Hotel(
                "Armenia Hotel",
                "Categoría del hotel:3,5 estrella(s), Cantidad de habitaciones: 18 ",
                "Una excelente opción en Armenia, el hotel es amplio y muy bien atendido por todo el personal. Las habitaciones son bastante cómodas y muy bien dotadas par tener una agradable estadía. La estadía incluye un desayuno tipo bufete que está muy bien balanceado y ofrece gran variedad.  Dirección: Avenida Bolivar 8",
                "4.624838213794354",
                "-75.762595160852",
                "Dirección: Avenida Bolivar 8",
                R.drawable.armenia));

        loadHotel(db, new ModeloHotel.Hotel(
                "Decameron Las Heliconias",
                "Categoría del hotel:3,5 estrella(s),Cantidad de habitaciones: 34",
                "Este hotel no solamente cuenta con una ambientación increíble sino además es parte de la naturaleza de Armenia. La atención es muy buena, la comida excelente y además entrada a panaca ilimitada, Dirección: Km. 7 Vereda Kerman | Parque Nacional de la Cultura Agropecuaria, Quimbaya, Colombia",
                "4.621929466163072",
                "-75.76083563173823",
                "Dirección: Km 2 Vía a Panaca, Vereda Kerman, Quimbaya ",
                R.drawable.heliconias));
    }

    private long loadHotel(SQLiteDatabase db, ModeloHotel.Hotel hotel)
    {
        return db.insert(Tables.HOTEL,null, hotel.toContentValues());
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion)
    {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + Tables.HOTEL);
        this.onCreate(sqLiteDatabase);
    }
}

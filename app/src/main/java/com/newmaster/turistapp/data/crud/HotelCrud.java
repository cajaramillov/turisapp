package com.newmaster.turistapp.data.crud;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.newmaster.turistapp.data.TurisAppDBHelper.Tables;
import com.newmaster.turistapp.data.TurisAppContract.HotelEntity;

import com.newmaster.turistapp.data.TurisAppDBHelper;

public class HotelCrud
{
    private static TurisAppDBHelper dataBase;
    private static HotelCrud instance = new HotelCrud();

    public HotelCrud() {}

    public static HotelCrud getInstance(Context contexto)
    {
        if (dataBase == null)
        {
            dataBase = new TurisAppDBHelper(contexto);
        }

        return instance;
    }

    public Cursor buscarHoteles()
    {
        SQLiteDatabase db = dataBase.getReadableDatabase();
        String sql = String.format("SELECT * FROM %s ORDER BY %s", Tables.HOTEL, HotelEntity.NOMBRE);
        return db.rawQuery(sql, null);
    }

    public Cursor buscarHotel(String idHotel)
    {
        SQLiteDatabase db = dataBase.getReadableDatabase();
        String sql = String.format("SELECT * FROM %s WHERE %s=%s", Tables.HOTEL, HotelEntity._ID, idHotel);
        return db.rawQuery(sql, null);
    }
}
